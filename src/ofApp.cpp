#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    //build a cube of objects with all three axis
    for(int x = 0; x< 10; x++){
        for(int y = 0; y< 10; y++){
            for(int z = 0; z< 10; z++){
                int currOrb = ((x*10)+y)*10+z; //calculate the index for the orb
                orbs[currOrb].setPosition(x*20, y*20, z*20); //position the orb
                orbs[currOrb].setRadius(5); //set the size of the orbs
            }
        }
    }
    //Camera Position
    cam.setPosition(100, 100, 300);
    
    //setup a warm light
    light1.setDiffuseColor(ofColor(255, 250, 200));
    light1.setPosition(-100, 100, 500);
    light1.enable();
    
    //setup a cool light
    light2.setDiffuseColor(ofColor(155, 150, 230));
    light2.setPosition(-400, -200, 000);
    light2.enable();
}

//--------------------------------------------------------------
void ofApp::update(){
    noiseTime+=0.03; //move our timer forward
    //use our 3 axis grid again so we have the positions of each orb
    for(int x = 0; x< 10; x++){
        for(int y = 0; y< 10; y++){
            for(int z = 0; z< 10; z++){
                int currOrb = ((x*10)+y)*10+z; //calculate the index again
                float myNoise = 25.0*ofSignedNoise(x/8.0, y/8.0-noiseTime, z/8.0, noiseTime); //figure out the noise at those coordinates
                if(myNoise<0)myNoise = 0; //do not use the negative noise
                orbs[currOrb].setRadius(myNoise); //change the size of the orb with the noise
            }
        }
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
    
    ofEnableDepthTest(); //sort the drawing so that the things closest to the camera is in front
    cam.begin();
    ofRotateDeg(45, 0, 1, 0); //rotate the cube 45 degrees on the y-axis
    
    for(int i = 0; i<1000; i++){
        ofSetColor((orbs[i].getRadius()*6.0), 200);//set the color so they are brighter as they are bigger
        orbs[i].draw();//draw all the orbs
    }
    cam.end();
    ofDisableDepthTest();
    
    cam.draw(); //draw whatever the camera saw
}
